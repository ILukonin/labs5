#include "MyString.h"


int input_str(char ** str,FILE *file)
{
	char * temp = *str;
	int i = 0;

	*str = (char *)calloc(1,sizeof(char)); // выделяем память под один чар
    if( (*str) == NULL)
	{
		printf("\nCannot allocate string!\n");
		return 0;
	}
    //fgetc(file);//без этого читался символ с номером 10 - линия свободна. в LIN не работает без него. в WIN  - с ним не работает
	do
	{

        (*str)[i] = fgetc(file); //да-да, тут будет каша из указателей и индексов =(

        if( (*str)[i] == '\n' )
		{
			(*str)[i] = '\0';
			break;
		}

		i++;
		temp = NULL;
		temp = (char *)realloc(*str,sizeof(char)*(i+1)); //добавим ещё один элемент в массив для записи конца строки. Да я знаю, что это медленно и нужно выделять блоками. Но пусть будет пока так.

		if(temp == NULL) // Используем временный указатель чтоб не было утечек. Потому как паямть освобождается только в том случае, если перевыделение было удачным, иначе указатель обнулится
		{
			printf("\nCannot reallocate string!\n");
			return 0;
		} else
		{
			*str = temp;
			temp = NULL;
			(*str)[i] = '\0';
		}



	} while (!NULL);


	return i;
};


void print_word(const WORD_PTR word, FILE * file)
{
	char * tmp = word.start;

	while(tmp <= word.end)
	{
		putc(*tmp,file);
		tmp++;
	}

	return;
}


int getint(char * message)
{
    int i = 0;
    int res = 0;

    printf("%s\n", message);
    res = scanf("%d",&i);

    if (res < 1 )
    {
        printf("Try again!\n");
        i = 0;

    }


    return i;
}

int input_txt(char *** text, FILE *file)
{
	char ** temp = *text;
	char * tmp_str = NULL;
	int i = 0;
	int count = 0;

	*text = (char **)calloc(1,sizeof(char *)); //так-то это массив указателей на указатели. так мы инициализируем нулевыми указател¤ми


	if( *text == NULL)
	{
		printf("\ncan't allocate text\n");
		return 0;
	}

	do
	{

		count = input_str( &((*text)[i]), file); //пишем строку. параметр &((*text)[i]) выгл¤дит довольно жутко

		if(count != 0 ) 
		{
			temp = NULL;
			temp = (char **)realloc(*text, sizeof(char *)*(i+2)); //выдел¤ем пам¤ть на новый указатель =(

			if( temp == NULL)
			{
				printf("\ncan't realocate line!\n");
				break;
			} else
			{
				*text = temp;
				
			}
		}

		

		i++;

	}while( count != 0 );

	return i;
}

void print_sort(char ** text, int count, FILE * file) //не будем думать о сохранности исходного текста. лениво.
{
	unsigned int lenght = 0;
	int i = 0;
	char * str = NULL;

	while( !NULL )
	{
		lenght = strlen(text[0]);
		str = text[0];
		for(i = 0; i < count; i++)
		{
			if( strlen(text[i]) > lenght)
			{
				lenght = strlen(text[i]);
				str = text[i];
			}
		}
		fprintf(file,"%s\n", str);
		*str = '\0'; //затираем строчку
		if(lenght == 0)
			break; //выходим, если так и не нашли ненулевой строки
	}

}

int scan_word(char * str, WORD_PTR ** word_lst)
{
	int lenght = strlen(str);
	int i = 0;
	int count = 0;
	WORD_PTR * tmp_ptr= NULL;

	tmp_ptr = (WORD_PTR *)calloc(1,sizeof(WORD_PTR));
	if(tmp_ptr == NULL)
	{
		printf("can't allocate mem for word list\n");
		return 0;
	}

	*word_lst = tmp_ptr;

	if(str[0] != ' ')
	{
		(*word_lst)[0].start = &(str[0]); // если начинается не с пробела
	}



	for(i=1;i<lenght;i++)
	{
		if(  (str[i] != ' ')&&(str[i-1] == ' ' ) )
		{
			(*word_lst)[count].start = &(str[i]);//запонили начало слова
		
		}

		if(  (str[i] == ' ')&&(str[i-1] != ' ' ))
		{
			(*word_lst)[count].end = &(str[i-1]);//запонили конец слова
			
			tmp_ptr = (WORD_PTR * )realloc(*word_lst, sizeof(WORD_PTR)*(count+2));//добавили ещё элемент в массив
			if(tmp_ptr == NULL)
			{
				printf("can't reallocate mem for word list\n");
				return 0;
			}

			*word_lst = tmp_ptr;
			count++;
		}

	}

	if(str[lenght] != ' ')
		(*word_lst)[count].end = &(str[lenght-1]); // если заканчивается не пробелом

	return count+1;
}


void randomize_word( WORD_PTR ** word_lst, int count)
{
	WORD_PTR * tmp = NULL; //массив для перемешивания
	int * index = NULL; //массив индексов
	int i = 0;
	int j = 0;
	int flag = 0;

	tmp = (WORD_PTR *)calloc(count,sizeof(WORD_PTR));
	index = (int *)calloc(count,sizeof(int));

	for(i = 0; i < count; i++)
		index[i] = 0;

	i = 0;

	while( i < count)
	{
		flag = 0;
		index[i] = rand() % count;
		for(j=0; j < i; j++)
			if( index[j] == index[i])
				flag = 1;
		if( flag == 0)
			i++;
	}

	for(i = 0; i < count; i++)
		tmp[i] = (*word_lst)[index[i]];
	
	free(*word_lst); //не забываем почистить
	*word_lst = tmp;

	return;
}

int lenght_word(WORD_PTR  word)
{
	int i = 0;
	char * ptr = word.start;
	while( ptr <= word.end)
	{
		ptr++;
		i++;
	}

	return i;
}

void copi_word(WORD_PTR * source, WORD_PTR * dest)
{
	char * source_tmp = source->start;
	char * dest_tmp = dest->start;

	while(dest_tmp <= dest->end)
	{
		*dest_tmp = *source_tmp;
		dest_tmp++;
		source_tmp++;

		if(source_tmp > source->end)
			break;

	}

	return;
}

void shake_word(WORD_PTR * word)
{
	char * tmp = NULL;
	int * index = NULL;
	int i = 0;
	int j = 0;
	int flag = 0;
	int count = lenght_word(*word);
	


	tmp = (char *)calloc(count,sizeof(char));
	index = (int *)calloc(count,sizeof(int));

	for(i = 0; i < count; i++)
		index[i] = 0;

	i = 0;

	while( i < count)
	{
		flag = 0;
		index[i] = rand() % count;
		for(j=0; j < i; j++)
			if( index[j] == index[i])
				flag = 1;
		if( flag == 0)
			i++;
	}

	for(i = 0; i < count; i++)
		tmp[i] = (word->start)[index[i]];
	
	
	word->start = &(tmp[0]);
	word->end = &(tmp[count - 1]);

	return;
}