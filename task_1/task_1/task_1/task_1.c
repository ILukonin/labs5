// task_1.c: ���������� ����� ����� ��� ����������� ����������.

#include "MyString.h"
#include <time.h>






int main()
{
	char * str = NULL;
	int count = 0;
	int i = 0;
	WORD_PTR * word_lst = NULL;
	FILE * file_in = stdin;
	FILE * file_out = stdout;

	srand(time(NULL));


	printf("input string\n");
	count = input_str(&str,file_in);
	if(count < 1)
	{
		printf("invalid string!\n");
		return 1;
	}

	count = scan_word(str, &word_lst);

	randomize_word( &word_lst, count);

	for( i = 0; i < count; i++)
	{
		print_word(word_lst[i],file_out);
		fputc(' ',file_out);

	}

	fputc('\n',file_out);

	free(word_lst);
	free(str);
	return 0;
}

