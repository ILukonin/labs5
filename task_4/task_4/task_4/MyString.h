#pragma once

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>


typedef struct
{
	char * start;
	char * end;
} WORD_PTR;

int input_str(char ** str,FILE *file); //тут будем вводить строку откуда-нибудь. Возвращаем количество введённых символов.
void print_word(const WORD_PTR word, FILE * file);
int getint(char * message); // функция для ввода целых положительных чисел
int input_txt(char *** text,FILE *file); // а тут - массив строк. так же вернём количество строк. читаем из файла до первой попавшейся пустой строки, а не до конца. в файле обязательн должна быть хотя бы одна пустая строка.
void print_sort(char ** text, int count, FILE * file); //выводит текст куда-нибудь. не будем думать о сохранности исходного текста. лениво.
int scan_word(char * str, WORD_PTR ** word_lst); // формирует из строки массив указателей на слова. возвращает количество слов.
int lenght_word(WORD_PTR  word);//узнаем длину слова

void randomize_word( WORD_PTR ** word_lst, int count); //перемешиваем слова в массиве
void shake_word(WORD_PTR * word); //перемещивание слова
void copi_word(WORD_PTR * source, WORD_PTR * dest); //копирование одного слова в другое физически