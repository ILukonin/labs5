// task_2.c: ���������� ����� ����� ��� ����������� ����������.
//

#include "MyString.h"

void generate(unsigned short int *** canvas, int weight, int height)
{
	int i = 0;
	*canvas = (unsigned short int **)calloc( height, sizeof(unsigned short int *));
	for(i = 0; i < height; i++)
		(*canvas)[i] = (unsigned short int *)calloc( weight, sizeof(unsigned short int ) );
	
	return;

}

void free_canvas(unsigned short int *** canvas, int weight, int height)
{
	int i = 0;
	for(i = 0; i < height; i++)
			free((*canvas)[i]);

	free(*canvas);
	return;
}

void rand_canvas(unsigned short int ** canvas, int weight, int height)
{
	int i = 0;
	int j = 0;

	for(i = 0; i < height/2; i++) //��������� ����� ������� ��������
		for(j = 0; j < weight/2; j++)
			canvas[i][j] = rand()%2;


	for(i = height/2; i < height; i++) //����� ������ ��������
		for(j = 0; j < weight/2; j++)
			canvas[i][j] = canvas[i - height/2][j]; 

	for(i = height/2; i<height;i++) // ������ ������ ���������
		for(j = weight/2; j < weight; j++)
			canvas[i][j] = canvas[i - height/2][j-weight/2]; 

	for(i = 0; i < height/2; i++) // ������ ������� ���������
		for(j = weight/2; j < weight; j++)
			canvas[i][j] = canvas[i][j-weight/2]; 

	return;
}

void clear_canvas(unsigned short int ** canvas, int weight, int height)
{
	int i = 0;
	int j = 0;

	for(i = 0; i < height; i++)
		for(j = 0; j < weight; j++)
			canvas[i][j] = 0;

	return;
}

void print_canvas(unsigned short int ** canvas, int weight, int height)
{
	int i = 0;
	int j = 0;

	for(i=0;i < height;i++)
	{
		for(j=0;j < weight;j++)
		{
			if(canvas[i][j] == 0)
				putchar(' ');
			else
				putchar('*');
		}
		putchar('\n');
	}

	return;
}

int main()
{
	int weight = 0;
	int height = 0;
	unsigned short int ** canvas; //����� ���� �� ����� � ������. ��� ���� - ��� ��������.


	srand(time(NULL));

	weight = getint("input weight");
	if(weight < 1)
	{
		printf("bed weight!\n");
		return 1;
	}

	height = getint("input height");
	if(height < 1)
	{
		printf("bed height!\n");
		return 1;
	}

	generate(&canvas,weight,height);

	while( !NULL ) //���� ��������
	{
	clock_t tmp = clock();

	clear_canvas(canvas,weight,height);
	rand_canvas(canvas,weight,height);

	print_canvas(canvas,weight,height);

	while(clock() < (tmp + CLOCKS_PER_SEC)  ); // ������ �������� � �������

	}

	free_canvas(&canvas,weight,height);
	return 0;
}

